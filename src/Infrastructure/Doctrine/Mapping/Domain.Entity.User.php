<?php

declare(strict_types=1);

use Doctrine\DBAL\Types\Types;
use Domain\Enums\UserRole;

$metadata->mapField(array(
    'id' => true,
    'fieldName' => 'id',
    'type' => Types::STRING
));

$metadata->mapField(array(
    'fieldName' => 'role',
    'type' => Types::STRING,
    'options' => array(
        'default' => UserRole::ROLE_USER->value
    )
));

$metadata->mapField(array(
    'fieldName' => 'email',
    'type' => Types::STRING
));

$metadata->mapField(array(
    'fieldName' => 'phoneNumber',
    'type' => Types::STRING,
));

$metadata->mapField(array(
    'fieldName' => 'firstName',
    'type' => Types::TEXT
));

$metadata->mapField(array(
    'fieldName' => 'lastName',
    'type' => Types::INTEGER
));

$metadata->mapField(array(
    'fieldName' => 'password',
    'type' => Types::INTEGER
));

$metadata->mapField(array(
    'fieldName' => 'properties',
    'type' => Types::JSON,
    'nullable' => true
));

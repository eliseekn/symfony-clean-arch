<?php

declare(strict_types=1);

use Doctrine\DBAL\Types\Types;
use Doctrine\DBAL\Types\Type;
use Infrastructure\Doctrine\Types\TitleType;

Type::addType(TitleType::NAME, TitleType::class);

$metadata->mapField(array(
    'id' => true,
    'fieldName' => 'id',
    'type' => Types::STRING
));

$metadata->mapField(array(
    'fieldName' => 'title',
    'type' => TitleType::NAME
));

$metadata->mapField(array(
    'fieldName' => 'slug',
    'type' => Types::STRING,
    'nullable' => true
));

$metadata->mapField(array(
    'fieldName' => 'description',
    'type' => Types::TEXT
));

$metadata->mapField(array(
    'fieldName' => 'price',
    'type' => Types::INTEGER
));

$metadata->mapField(array(
    'fieldName' => 'area',
    'type' => Types::INTEGER
));

$metadata->mapField(array(
    'fieldName' => 'rooms',
    'type' => Types::INTEGER
));

$metadata->mapField(array(
    'fieldName' => 'baths',
    'type' => Types::INTEGER
));

$metadata->mapField(array(
    'fieldName' => 'location',
    'type' => Types::STRING
));

$metadata->mapField(array(
    'fieldName' => 'category',
    'type' => Types::STRING
));

$metadata->mapField(array(
    'fieldName' => 'available',
    'type' => Types::BOOLEAN
));

$metadata->mapField(array(
    'fieldName' => 'features',
    'type' => Types::JSON
));

$metadata->mapField(array(
    'fieldName' => 'cover',
    'type' => Types::STRING
));

$metadata->mapField(array(
    'fieldName' => 'photos',
    'type' => Types::JSON
));

$metadata->mapField(array(
    'fieldName' => 'createdAt',
    'type' => Types::DATETIME_IMMUTABLE
));

$metadata->mapField(array(
    'fieldName' => 'userId',
    'type' => Types::STRING
));

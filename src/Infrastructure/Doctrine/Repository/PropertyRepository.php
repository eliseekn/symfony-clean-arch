<?php

declare(strict_types=1);

namespace Infrastructure\Doctrine\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Domain\Entity\Property;
use Domain\Repository\Property\PropertyRepositoryInterface;

final class PropertyRepository extends ServiceEntityRepository implements PropertyRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Property::class);
    }

    public function add(Property $property): void
    {
        $this->_em->persist($property);
        $this->_em->flush();
    }

    public function remove(Property $property): void
    {
        $this->_em->remove($property);
        $this->_em->flush();
    }

    public function get(string $id): Property
    {
        return $this->find($id);
    }

    public function getAll(): array
    {
        return $this->findAll();
    }
}

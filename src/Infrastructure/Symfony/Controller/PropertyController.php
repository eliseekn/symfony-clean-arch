<?php

declare(strict_types=1);

namespace Infrastructure\Symfony\Controller;

use Domain\UseCase\Property\StoreUseCase;
use Infrastructure\Doctrine\Repository\PropertyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class PropertyController extends AbstractController
{
    #[Route(path: '/api/properties', methods: 'POST')]
    public function store(Request $request, PropertyRepository $propertyRepository, StoreUseCase $storeUseCase): Response
    {
        $storeUseCase->handle($propertyRepository, $request->request->all());

        return $this->json([
            'status' => 'success',
            'message' => 'Property stored successfully',
        ]);
    }
}

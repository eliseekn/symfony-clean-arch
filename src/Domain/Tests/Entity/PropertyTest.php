<?php

declare(strict_types=1);

namespace Domain\Tests\Entity;

use Domain\Entity\Property;
use DateTimeImmutable;
use Domain\Factory\PropertyFactory;
use Domain\Factory\UserFactory;
use Domain\Tests\DataFixture;
use PHPUnit\Framework\TestCase;

final class PropertyTest extends TestCase
{
    use DataFixture;

    public function testCreateInstanceFromEntityIsSuccessful(): void
    {
        $id = uniqid();
        $now = new DateTimeImmutable();

        $property = new Property(
            id: $id,
            title: 'Lorem ipsum dolor',
            slug: 'lorem-ipsum-dolor',
            description: 'Lorem ipsum dolor sit amet',
            price: 1000,
            area: 20,
            baths: 2,
            rooms: 2,
            location: 'Lorem ipsum',
            category: 'Lorem',
            available: true,
            features: json_encode(['features']),
            cover: 'cover',
            photos: json_encode(['photos']),
            userId: $id,
            createdAt: $now
        );

        $this->assertNotNull($property);
        $this->assertInstanceOf(Property::class, $property);
        $this->assertEquals($id, $property->id);
        $this->assertEquals('Lorem ipsum dolor', $property->title);
        $this->assertEquals('lorem-ipsum-dolor', $property->slug);
        $this->assertEquals('Lorem ipsum dolor sit amet', $property->description);
        $this->assertEquals(1000, $property->price);
        $this->assertEquals(20, $property->area);
        $this->assertEquals(2, $property->baths);
        $this->assertEquals(2, $property->rooms);
        $this->assertEquals('Lorem ipsum', $property->location);
        $this->assertEquals('Lorem', $property->category);
        $this->assertEquals(true, $property->available);
        $this->assertEquals(json_encode(['features']), $property->features);
        $this->assertEquals('cover', $property->cover);
        $this->assertEquals(json_encode(['photos']), $property->photos);
        $this->assertEquals($now, $property->createdAt);
    }

    public function testUpdateInstanceFromEntityIsSuccessful(): void
    {
        $property = new Property(
            id: uniqid(),
            title: 'Lorem ipsum dolor',
            slug: 'lorem-ipsum-dolor',
            description: 'Lorem ipsum dolor sit amet',
            price: 1000,
            area: 20,
            baths: 2,
            rooms: 2,
            location: 'Lorem ipsum',
            category: 'Lorem',
            available: true,
            features: json_encode(['features']),
            cover: 'cover',
            photos: json_encode(['photos']),
            userId: uniqid(),
            createdAt: new DateTimeImmutable()
        );

        $property->title = 'Dolor ipsum lorem';
        $property->slug = 'dolor-ipsum-lorem';

        $this->assertNotNull($property);
        $this->assertEquals('Dolor ipsum lorem', $property->title);
        $this->assertEquals('dolor-ipsum-lorem', $property->slug);
    }

    public function testCreateInstanceFromFactoryIsSuccessful(): void
    {
        $user = UserFactory::create($this->getUserData());
        $property = PropertyFactory::create($this->getPropertyData($user->id));

        $this->assertNotNull($property);
        $this->assertInstanceOf(Property::class, $property);
        $this->assertEquals('Lorem ipsum dolor', $property->title);
        $this->assertEquals('lorem-ipsum-dolor', $property->slug);
        $this->assertEquals('Lorem ipsum dolor sit amet', $property->description);
        $this->assertEquals(1000, $property->price);
        $this->assertEquals(20, $property->area);
        $this->assertEquals(2, $property->baths);
        $this->assertEquals(2, $property->rooms);
        $this->assertEquals('Lorem ipsum', $property->location);
        $this->assertEquals('Lorem', $property->category);
        $this->assertEquals(true, $property->available);
        $this->assertEquals(json_encode(['features']), $property->features);
        $this->assertEquals('cover', $property->cover);
        $this->assertEquals(json_encode(['photos']), $property->photos);
    }

    public function testUpdateInstanceFromFactoryIsSuccessful(): void
    {
        $user = UserFactory::create($this->getUserData());
        $property = PropertyFactory::create($this->getPropertyData($user->id));
        $property = PropertyFactory::update($property, ['title' => 'Dolor ipsum lorem']);

        $this->assertNotNull($property);
        $this->assertInstanceOf(Property::class, $property);
        $this->assertEquals('Dolor ipsum lorem', $property->title);
    }
}

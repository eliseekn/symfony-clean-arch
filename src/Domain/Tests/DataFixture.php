<?php

declare(strict_types=1);

namespace Domain\Tests;

trait DataFixture
{
    private function getPropertyData(string $userId, array $attributes = []): array
    {
        return $attributes + [
            'title' => 'Lorem ipsum dolor',
            'slug' => 'lorem-ipsum-dolor',
            'description' => 'Lorem ipsum dolor sit amet',
            'price' => 1000,
            'area' => 20,
            'baths' => 2,
            'rooms' => 2,
            'location' => 'Lorem ipsum',
            'category' => 'Lorem',
            'available' => true,
            'features' => json_encode(['features']),
            'cover' => 'cover',
            'photos' => json_encode(['photos']),
            'userId' => $userId,
        ];
    }

    private function getUserData(array $attributes = []): array
    {
        return $attributes + [
            'email' => 'lorem@ipsum.dolor',
            'firstName' => 'Lorem ipsum',
            'lastName' => 'Dolor',
            'phoneNumber' => '0000000000',
            'password' => 'password',
            'properties' => json_encode(['properties'])
        ];
    }
}

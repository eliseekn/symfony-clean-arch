<?php

declare(strict_types=1);

namespace Domain\Tests\UseCase;

use Domain\Factory\UserFactory;
use Domain\Repository\Property\PropertyRepository;
use Domain\Entity\Property;
use Domain\Tests\DataFixture;
use Domain\UseCase\Property\DeleteUseCase;
use Domain\UseCase\Property\GetCollectionUseCase;
use Domain\UseCase\Property\GetItemUseCase;
use Domain\UseCase\Property\StoreUseCase;
use Domain\UseCase\Property\UpdateUseCase;
use PHPUnit\Framework\TestCase;
use Symfony\Component\String\Slugger\AsciiSlugger;

final class PropertyUseCaseTest extends TestCase
{
    use DataFixture;

    protected function setUp(): void
    {
        $this->propertyRepository = new PropertyRepository;
        $this->user = UserFactory::create($this->getUserData())->id;
        $this->storeUseCase = new StoreUseCase(new AsciiSlugger());
        $this->updateUseCase = new UpdateUseCase(new AsciiSlugger());
        $this->deleteUseCase = new DeleteUseCase;
        $this->getItemUseCase = new GetItemUseCase;
        $this->getCollectionUseCase = new GetCollectionUseCase;
    }

    public function testStoreUseCaseIsSuccessful(): void
    {
        $property = $this->storeUseCase->handle($this->propertyRepository, $this->getPropertyData($this->user));

        $this->assertInstanceOf(Property::class, $property);
        $this->assertArrayHasKey($property->id, $this->propertyRepository->properties);
        $this->assertEquals($property, $this->propertyRepository->properties[$property->id]);
    }

    public function testUpdateUseCaseIsSuccessful(): void
    {
        $property = $this->storeUseCase->handle($this->propertyRepository, $this->getPropertyData($this->user));
        $property = $this->updateUseCase->handle($this->propertyRepository, $property->id, ['title' => 'Dolor amet consecutor']);

        $this->assertInstanceOf(Property::class, $property);
        $this->assertArrayHasKey($property->id, $this->propertyRepository->properties);
        $this->assertEquals($property, $this->propertyRepository->properties[$property->id]);
        $this->assertEquals('Dolor amet consecutor', $property->title);
        $this->assertEquals('dolor-amet-consecutor', $property->slug);
    }

    public function testDeleteUseCaseIsSuccessful(): void
    {
        $property = $this->storeUseCase->handle($this->propertyRepository, $this->getPropertyData($this->user));
        $property = $this->deleteUseCase->handle($this->propertyRepository, $property->id);

        $this->assertArrayNotHasKey($property->id, $this->propertyRepository->properties);
    }

    public function testGetItemUseCaseIsSuccessful(): void
    {
        $property = $this->storeUseCase->handle($this->propertyRepository, $this->getPropertyData($this->user));
        $property = $this->getItemUseCase->handle($this->propertyRepository, $property->id);

        $this->assertInstanceOf(Property::class, $property);
        $this->assertArrayHasKey($property->id, $this->propertyRepository->properties);
        $this->assertEquals($property, $this->propertyRepository->properties[$property->id]);
    }

    public function testGetCollectionUseCaseIsSuccessful(): void
    {
        for ($i = 0; $i <= 4; $i++) {
            $this->storeUseCase->handle($this->propertyRepository, $this->getPropertyData($this->user));
        }

        $properties = $this->getCollectionUseCase->handle($this->propertyRepository);

        $this->assertIsArray($properties);
        $this->assertCount(5, $this->propertyRepository->properties);
    }
}

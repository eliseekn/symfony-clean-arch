<?php

declare(strict_types=1);

namespace Domain\Factory;

use Domain\Entity\User;
use DateTimeImmutable;
use Domain\Enums\UserRole;

abstract class UserFactory
{
    public static function create(array $attributes): User
    {
        return new User(
            uniqid(),
            $attributes['email'],
            $attributes['phoneNumber'],
            $attributes['password'],
            $attributes['firstName'],
            $attributes['lastName'],
            $attributes['properties'],
            UserRole::ROLE_USER->value,
            new DateTimeImmutable()
        );
    }

    public static function update(User $user, array $attributes): User
    {
        $user->email = $attributes['email'] ?? $user->email;
        $user->phoneNumber = $attributes['phoneNumber'] ?? $user->phoneNumber;
        $user->password = $attributes['password'] ?? $user->password;
        $user->firstName = $attributes['firstName'] ?? $user->firstName;
        $user->lastName = $attributes['lastName'] ?? $user->lastName;
        $user->properties = $attributes['properties'] ?? $user->properties;
        $user->role = $attributes['role'] ?? $user->role;

        return $user;
    }
}

<?php

declare(strict_types=1);

namespace Domain\Factory;

use Domain\Entity\Property;
use DateTimeImmutable;

abstract class PropertyFactory
{
    public static function create(array $attributes): Property
    {
        return new Property(
            uniqid(),
            $attributes['title'],
            $attributes['slug'],
            $attributes['description'],
            (int) $attributes['price'],
            (int) $attributes['area'],
            (int) $attributes['baths'],
            (int) $attributes['rooms'],
            $attributes['location'],
            $attributes['category'],
            (bool) $attributes['available'],
            $attributes['features'],
            $attributes['cover'],
            $attributes['photos'],
            $attributes['userId'],
            new DateTimeImmutable()
        );
    }

    public static function update(Property $property, array $attributes): Property
    {
        $property->title = $attributes['title'] ?? $property->title;
        $property->description = $attributes['description'] ?? $property->description;
        $property->price = $attributes['price'] ?? $property->price;
        $property->area = $attributes['area'] ?? $property->area;
        $property->baths = $attributes['baths'] ?? $property->baths;
        $property->rooms = $attributes['rooms'] ?? $property->rooms;
        $property->location = $attributes['location'] ?? $property->location;
        $property->category = $attributes['category'] ?? $property->category;
        $property->available = $attributes['available'] ?? $property->available;
        $property->features = $attributes['features'] ?? $property->features;
        $property->cover = $attributes['cover'] ?? $property->cover;
        $property->photos = $attributes['photos'] ?? $property->photos;

        return $property;
    }
}

<?php

namespace Domain\Enums;

enum UserRole: string
{
    case ROLE_ADMIN = 'admin';
    case ROLE_USER = 'user';
}

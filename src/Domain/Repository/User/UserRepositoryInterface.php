<?php

declare(strict_types=1);

namespace Domain\Repository\User;

use Domain\Entity\User;

interface UserRepositoryInterface
{
    public function add(User $user);

    public function remove(User $user);

    public function get(string $id): User;

    public function getAll(): array;
}

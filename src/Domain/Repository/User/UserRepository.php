<?php

declare(strict_types=1);

namespace Domain\Repository\User;

use Domain\Entity\User;

final class UserRepository implements UserRepositoryInterface
{
    public function __construct(public array $users = []) {}

    public function add(User $user)
    {
        $this->users[$user->id] = $user;
    }

    public function remove(User $user)
    {
        unset($this->users[$user->id]);
    }

    public function get($id): User
    {
        return $this->users[$id];
    }

    public function getAll(): array
    {
        return $this->users;
    }
}

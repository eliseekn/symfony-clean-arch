<?php

declare(strict_types=1);

namespace Domain\Repository\Property;

use Domain\Entity\Property;

final class PropertyRepository implements PropertyRepositoryInterface
{
    public function __construct(public array $properties = []) {}

    public function add(Property $property)
    {
        $this->properties[$property->id] = $property;
    }

    public function remove(Property $property)
    {
        unset($this->properties[$property->id]);
    }

    public function get($id): Property
    {
        return $this->properties[$id];
    }

    public function getAll(): array
    {
        return $this->properties;
    }
}

<?php

declare(strict_types=1);

namespace Domain\Repository\Property;

use Domain\Entity\Property;

interface PropertyRepositoryInterface
{
    public function add(Property $property);

    public function remove(Property $property);

    public function get(string $id): Property;

    public function getAll(): array;
}

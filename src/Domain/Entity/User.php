<?php

declare(strict_types=1);

namespace Domain\Entity;

use DateTimeImmutable;

final class User
{
    public function __construct(
        public ?string $id,
        public string $email,
        public string $phoneNumber,
        public string $password,
        public string $firstName,
        public string $lastName,
        public ?string $properties,
        public string $role,
        public DateTimeImmutable $createdAt
    ) {}
}

<?php

declare(strict_types=1);

namespace Domain\Entity;

use DateTimeImmutable;

final class Property
{
    public function __construct(
        public ?string $id,
        public string $title,
        public ?string $slug,
        public string $description,
        public int $price,
        public int $area,
        public int $baths,
        public int $rooms,
        public string $location,
        public string $category,
        public bool $available,
        public string $features,
        public string $cover,
        public string $photos,
        public string $userId,
        public DateTimeImmutable $createdAt
    ) {}
}

<?php

declare(strict_types=1);

namespace Domain\UseCase\Property;

use Domain\Repository\Property\PropertyRepositoryInterface;

final class GetCollectionUseCase
{
    public function handle(PropertyRepositoryInterface $propertyRepository): array
    {
        return $propertyRepository->getAll();
    }
}

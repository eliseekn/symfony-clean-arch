<?php

declare(strict_types=1);

namespace Domain\UseCase\Property;

use Domain\Repository\Property\PropertyRepositoryInterface;
use Domain\Entity\Property;

final class GetItemUseCase
{
    public function handle(PropertyRepositoryInterface $propertyRepository, string $id): Property
    {
        return $propertyRepository->get($id);
    }
}

<?php

declare(strict_types=1);

namespace Domain\UseCase\Property;

use Domain\Repository\Property\PropertyRepositoryInterface;
use Domain\Entity\Property;
use Domain\Factory\PropertyFactory;
use Symfony\Component\String\Slugger\SluggerInterface;

final class UpdateUseCase
{
    public function __construct(public SluggerInterface $slugger) {}

    public function handle(PropertyRepositoryInterface $propertyRepository, string $id, array $attributes): Property
    {
        $property = PropertyFactory::update($propertyRepository->get($id), $attributes);
        $property->slug = $this->slugger->slug(string: strtolower($property->title), locale: 'fr-FR')->toString();

        $propertyRepository->add($property);

        return $property;
    }
}

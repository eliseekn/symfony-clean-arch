<?php

declare(strict_types=1);

namespace Domain\UseCase\Property;

use Domain\Repository\Property\PropertyRepositoryInterface;
use DateTimeImmutable;
use Domain\Entity\Property;
use Domain\Factory\PropertyFactory;
use Symfony\Component\String\Slugger\SluggerInterface;

final class StoreUseCase
{
    public function __construct(public SluggerInterface $slugger) {}

    public function handle(PropertyRepositoryInterface $propertyRepository, array $attributes): Property
    {
        $property = PropertyFactory::create($attributes);
        $property->slug = $this->slugger->slug(string: strtolower($property->title), locale: 'fr-FR')->toString();
        $property->createdAt = new DateTimeImmutable();

        $propertyRepository->add($property);

        return $property;
    }
}

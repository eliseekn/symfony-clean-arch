<?php

declare(strict_types=1);

namespace Domain\UseCase\Property;

use Domain\Repository\Property\PropertyRepositoryInterface;
use Domain\Entity\Property;

final class DeleteUseCase
{
    public function handle(PropertyRepositoryInterface $propertyRepository, string $id): Property
    {
        $property = $propertyRepository->get($id);
        $propertyRepository->remove($property);

        return $property;
    }
}

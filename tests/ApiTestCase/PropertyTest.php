<?php

declare(strict_types=1);

namespace App\Tests\ApiTestCase;

use Domain\Entity\Property;
use Domain\Factory\UserFactory;
use Domain\Tests\DataFixture;
use Infrastructure\Doctrine\Repository\PropertyRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PropertyTest extends WebTestCase
{
    use DataFixture;

    public function testStoreUseCaseIsSuccessful(): void
    {
        $user = UserFactory::create($this->getUserData());
        $data = $this->getPropertyData($user->id);

        $client = static::createClient();
        $client->request('POST', '/api/properties', $data);
        $response = $client->getResponse()->getContent();

        $this->assertResponseIsSuccessful();
        $this->assertJson($response);
        $this->assertEquals([
            'status' => 'success',
            'message' => 'Property stored successfully',
        ], json_decode($response, true));

        /* @var PropertyRepository */
        $propertyRepository = static::getContainer()->get(PropertyRepository::class);
        $property = $propertyRepository->findOneBy(['title' => $data['title']]);

        $this->assertNotNull($property);
        $this->assertInstanceOf(Property::class, $property);
        $this->assertEquals($data['title'], $property->title);
    }
}

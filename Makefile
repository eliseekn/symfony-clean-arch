db:
	symfony console doctrine:database:drop --if-exists --no-interaction --force
	symfony console doctrine:database:create --if-not-exists --no-interaction
	symfony console make:migration
	symfony console doctrine:migrations:migrate --no-interaction

db-test: export APP_ENV=test
db-test:
	symfony console doctrine:database:drop --if-exists --no-interaction --force
	symfony console doctrine:database:create --if-not-exists --no-interaction
	symfony console doctrine:migrations:migrate --no-interaction

test: export APP_ENV=test
test:
	php bin/phpunit tests
